<?php
	$n = 10;
	for( $i = 0; $i < $n; $i++ ){
		$myMassiv[$i] = rand(0,20);
	}

	echo "Массив до сортировки: ";
	for( $i = 0; $i < $n; $i++ ){
		echo $myMassiv[$i] . "   ";
	}

	for( $i = 1; $i < $n; $i++){
		for( $j = $i; $j > 0 && $myMassiv[$j - 1] > $myMassiv[$j]; $j--){
			$x = $myMassiv[$j - 1];
			$myMassiv[$j - 1] = $myMassiv[$j];
			$myMassiv[$j] = $x;
		}
	}

	echo "</br> Массив после сортировки: ";
	for( $i = 0; $i < $n; $i++ ){
		echo $myMassiv[$i] . "   ";
	}
